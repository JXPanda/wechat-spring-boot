object Project {
    const val GROUP = "com.jxpanda.starter"
    const val VERSION = "1.3.8"
}

object ProjectDependency {

    const val AUTOCONFIGURE = ":wechat-spring-boot-autoconfigure"
    const val STARTER = ":wechat-spring-boot-starter"

}