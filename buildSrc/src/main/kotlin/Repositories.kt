import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.kotlin.dsl.maven
import java.net.URI

object Repositories {

    private const val DOMAIN = "https://nexus.jxpanda.com"
    private const val ALIYUN = "https://maven.aliyun.com/repository/public"
    private const val URL_RELEASE = "$DOMAIN/repository/maven-releases/"
    private const val URL_SNAPSHOTS = "$DOMAIN/repository/maven-snapshots/"
    private const val USERNAME = "ezor"
    private const val PASSWORD = "Ezor2019@)!("

    fun setRepositories(repositoryHandler: RepositoryHandler) {
        repositoryHandler.mavenLocal()
        repositoryHandler.maven(ALIYUN)
        repositoryHandler.maven(URL_RELEASE) {
            credentials {
                username = USERNAME
                password = PASSWORD
            }
        }
        repositoryHandler.maven(URL_SNAPSHOTS) {
            credentials {
                username = USERNAME
                password = PASSWORD
            }
        }
    }

    fun publishRepository(repositoryHandler: RepositoryHandler, version: String) {
        repositoryHandler.maven {
            url = URI(if (version.endsWith("SNAPSHOT")) URL_SNAPSHOTS else URL_RELEASE)
            credentials {
                username = USERNAME
                password = PASSWORD
            }
        }
    }

}