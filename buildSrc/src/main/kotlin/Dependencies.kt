object Version {
    const val SPRING = "2.5.8"

    /**
     * Panda的二方库，主要封装了常用的工具类的实体类
     * 内部依赖了jackson，所以这边不用显式的再依赖jackson了
     * */
    const val PANDA_COMMONS = "1.2.4"
    const val WECHAT_PAY_SDK = "0.2.2"
    const val APACHE_HTTP_CLIENT = "4.5.12"
    const val OK_HTTP3 = "4.9.1"
    const val REACTOR = "3.4.8"
    const val LOG4J2 = "2.15.0"
    const val LOGBACK = "1.2.7"
    const val SLF4J = "1.7.32"

}

object Library {
    /**
     * 工具类
     * */
    const val PANDA_TOOLKIT = "com.jxpanda.commons:commons-toolkit:${Version.PANDA_COMMONS}"

    /**
     * 加密/解密的工具
     * */
    const val PANDA_ENCRYPT = "com.jxpanda.commons:commons-encrypt:${Version.PANDA_COMMONS}"

    /**
     * 微信支付SDK V3
     * */
    const val WECHAT_PAY_SDK = "com.github.wechatpay-apiv3:wechatpay-apache-httpclient:${Version.WECHAT_PAY_SDK}"

    /**
     * Apache Httpclient
     * */
    const val APACHE_HTTP_CLIENT = "org.apache.httpcomponents:httpclient:${Version.APACHE_HTTP_CLIENT}"

    /**
     * OK HTTP 3
     * */
    const val OK_HTTP3 = "com.squareup.okhttp3:okhttp:${Version.OK_HTTP3}"

    /**
     * https://mvnrepository.com/artifact/io.projectreactor/reactor-core
     * */
    const val REACTOR = "io.projectreactor:reactor-core:${Version.REACTOR}"

    const val LOG4J2 = "org.apache.logging.log4j:log4j-core:${Version.LOG4J2}"
    const val LOG4J2_SLF4J = "org.apache.logging.log4j:log4j-slf4j-impl:${Version.LOG4J2}"

    const val LOGBACK_CORE = "ch.qos.logback:logback-core:${Version.LOGBACK}"
    const val LOGBACK_CLASSIC = "ch.qos.logback:logback-classic:${Version.LOGBACK}"
    const val SLF4J = "org.slf4j:slf4j-api:${Version.LOGBACK}"

}

/**
 * spring的依赖
 * 分开写是为了避免眼花
 * */
object SpringLibrary {
    const val SPRING_BOOT = "org.springframework.boot:spring-boot-starter:${Version.SPRING}"
    const val CONFIGURATION_PROCESSOR = "org.springframework.boot:spring-boot-configuration-processor:${Version.SPRING}"
}