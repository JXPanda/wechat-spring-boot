@file:Suppress("UnstableApiUsage")

plugins {
    java
    id("maven-publish")
    id("io.freefair.lombok") version "5.3.3.3"
}


allprojects {

    group = Project.GROUP
    version = Project.VERSION

    repositories {
        Repositories.setRepositories(this)
    }

}

subprojects {

    apply(plugin = "idea")
    apply(plugin = "java")
    apply(plugin = "java-library")
    apply(plugin = "maven-publish")
    apply(plugin = "io.freefair.lombok")

    java {
        sourceCompatibility = JavaVersion.VERSION_11
        withSourcesJar()
    }

    publishing {

        repositories {
            Repositories.publishRepository(this, version.toString())
        }

        publications {
            create<MavenPublication>("mavenJava") {
                from(components["java"])
                groupId = Project.GROUP
                artifactId = tasks.jar.get().archiveBaseName.get()
            }
        }

    }

}