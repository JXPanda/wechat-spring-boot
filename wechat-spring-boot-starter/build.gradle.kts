dependencies{
    api(SpringLibrary.SPRING_BOOT)
    api(project(ProjectDependency.AUTOCONFIGURE))
    api(Library.PANDA_TOOLKIT)
    api(Library.PANDA_ENCRYPT)
    api(Library.WECHAT_PAY_SDK)
}