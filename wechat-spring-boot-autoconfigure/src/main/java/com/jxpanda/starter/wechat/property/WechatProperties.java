package com.jxpanda.starter.wechat.property;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jxpanda.commons.toolkit.RSAKit;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.InputStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/**
 * @author Panda
 */
@Data
@SuppressWarnings("ALL")
@ConfigurationProperties(prefix = WechatProperties.PREFIX)
public class WechatProperties {

    public static final String PREFIX = "wechat";

    /**
     * 小程序的配置
     */
    private Application applet;

    /**
     * 商户号的配置，微信支付相关
     */
    private Merchant merchant;

    @Data
    public static class Application {
        /**
         * appId
         */
        private String appId;
        /**
         * appSecret
         */
        private String appSecret;

    }

    /**
     * 商户号的配置，与微信支付有关
     */
    @Data
    public static class Merchant {

        /**
         * 是否启用支付模块，默认false
         */
        private boolean enabled = false;
        /**
         * 商户号
         */
        private String mchId;
        /**
         * 商户key
         */
        private String mchKey;
        /**
         * 支付接口的回调地址（默认的，在调用接口的过程中可以修改）
         */
        private NotifyURL notifyUrl;
        /**
         * 微信支付的api V3 key
         */
        private String apiV3Key;
        /**
         * 商户私钥（约定，可以传递文件路径或者直接传递文本，通过判定是否是【-----BEGIN PRIVATE KEY-----】开头来判断是一个路径还是私钥）
         */
        private String privateKey;
        /**
         * 商户证书（约定，可以传递文件路径或者直接传递文本，通过判定是否是【-----BEGIN CERTIFICATE-----】开头来判断是一个路径还是证书）
         */
        private String certificate;
        /**
         * 证书序列号
         */
        private String certificateSerialNumber;

        @JsonIgnore
        private PrivateKey loadedPrivateKey;

        @JsonIgnore
        private X509Certificate loadedCertificate;

        @SneakyThrows
        public PrivateKey loadPrivateKey() {
            if (loadedPrivateKey == null) {
                loadedPrivateKey = RSAKit.keyHelper().loadPrivateKey(privateKey);
            }
            return loadedPrivateKey;
        }

        @SneakyThrows
        public X509Certificate loadCertificate() {
            InputStream inputStream;
            if (loadedCertificate == null) {
                loadedCertificate = RSAKit.keyHelper().loadCertificate(certificate);
            }
            return loadedCertificate;
        }

    }

    /**
     * 回调地址对象
     */
    @Data
    public static class NotifyURL {
        /**
         * 支付回调地址
         */
        private String pay;
        /**
         * 退款回调地址
         */
        private String refund;
    }

}
