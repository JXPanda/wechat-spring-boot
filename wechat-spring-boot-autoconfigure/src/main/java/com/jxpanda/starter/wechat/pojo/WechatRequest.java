package com.jxpanda.starter.wechat.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jxpanda.commons.toolkit.StringKit;
import lombok.Data;

/**
 * @author Panda
 */
@Data
@SuppressWarnings("AlibabaClassNamingShouldBeCamel")
public class WechatRequest {

    /**
     * 微信注册流程使用的对象结构
     */
    @Data
    public static class Register {
        /**
         * 后端加密后的session
         * 这个session解密后用得到的是
         *
         * @see WechatResponse.Session 对象
         * 这个对象中的sessionKey是用来解密下面两个密文用的
         */
        private String encryptedSession;

        /**
         * 用户数据密文
         */
        private EncryptedData userProfile;

        /**
         * 用户手机号密文
         */
        private EncryptedData userPhone;

        /**
         * 可选项，前端可以单独传递手机号进来注册
         * 此时不会去尝试解密userPhone数据
         */
        private String phone;

        /**
         * 手机号对应的验证码
         */
        private String captcha;

        /**
         * encryptedSession解密之后的对象
         */
        @JsonIgnore
        private WechatResponse.Session session;

        @JsonIgnore
        public WechatResponse.Session getSession() {
            if (session == null && StringKit.isNotBlank(encryptedSession)) {
                session = WechatResponse.Session.decrypt(encryptedSession);
            }
            return session;
        }
    }

    /**
     * 微信的密文对象
     */
    @Data
    public static class EncryptedData {
        /**
         * 密文
         */
        private String encryptedData;
        /**
         * 密文对应的iv
         */
        private String iv;
    }

    /**
     * 微信登录用的入参结构
     */
    @Data
    public static class Login {
        private String code;
    }

    @Data
    public static class QRCode {
        @JsonProperty("scene")
        private String scene;
    }

}
