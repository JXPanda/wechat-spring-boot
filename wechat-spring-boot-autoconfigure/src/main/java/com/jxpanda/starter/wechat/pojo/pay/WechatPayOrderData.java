package com.jxpanda.starter.wechat.pojo.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Panda
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WechatPayOrderData {

    @JsonProperty("appid")
    private String appId;
    @JsonProperty("mchid")
    private String mchId;
    @JsonProperty("transaction_id")
    private String transactionId;
    @JsonProperty("out_trade_no")
    private String outTradeNo;
    @JsonProperty("out_refund_no")
    private String outRefundNo;
    @JsonProperty("trade_type")
    private TradeType tradeType;
    @JsonProperty("trade_state")
    private TradeState tradeState;
    @JsonProperty("trade_state_desc")
    private String tradeStateDesc;
    @JsonProperty("bank_type")
    private String bankType;
    @JsonProperty("attach")
    private String attach;
    @JsonProperty("success_time")
    private String successTime;
    @JsonProperty("payer")
    private WechatPayer payer;
    @JsonProperty("amount")
    private WechatPayAmount amount;
    @JsonProperty("scene_info")
    private SceneInfo sceneInfo;

    public enum TradeType {
        /**
         * 公众号支付
         */
        JSAPI,
        /**
         * 扫码支付
         */
        NATIVE,
        /**
         * APP支付
         */
        APP,
        /**
         * 付款码支付
         */
        MICROPAY,
        /**
         * H5支付
         */
        MWEB,
        /**
         * 刷脸支付
         */
        FACEPAY;
    }

    public enum TradeState {
        /**
         * 支付成功
         */
        SUCCESS,
        /**
         * 转入退款
         */
        REFUND,
        /**
         * 未支付
         */
        NOTPAY,
        /**
         * 已关闭
         */
        CLOSED,
        /**
         * 已撤销（仅付款码支付会返回）
         */
        REVOKED,
        /**
         * 用户支付中（仅付款码支付会返回）
         */
        USERPAYING,
        /**
         * 支付失败（仅付款码支付会返回）
         */
        PAYERROR;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SceneInfo {
        @JsonProperty("device_id")
        private String deviceId;
    }

}
