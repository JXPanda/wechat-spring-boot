package com.jxpanda.starter.wechat.pojo.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Panda
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WechatPayRefundData {

    /**
     * mchid
     */
    @JsonProperty("mchid")
    private String mchid;
    /**
     * transactionId
     */
    @JsonProperty("transaction_id")
    private String transactionId;
    /**
     * outTradeNo
     */
    @JsonProperty("out_trade_no")
    private String outTradeNo;
    /**
     * refundId
     */
    @JsonProperty("refund_id")
    private String refundId;
    /**
     * outRefundNo
     */
    @JsonProperty("out_refund_no")
    private String outRefundNo;
    /**
     * refundStatus
     */
    @JsonProperty("refund_status")
    private RefundStatus refundStatus;
    /**
     * successTime
     */
    @JsonProperty("success_time")
    private String successTime;
    /**
     * userReceivedAccount
     */
    @JsonProperty("user_received_account")
    private String userReceivedAccount;
    /**
     * amount
     */
    @JsonProperty("amount")
    private WechatPayAmount amount;

    public enum RefundStatus {
        /**
         * 退款成功
         */
        SUCCESS,
        /**
         * 退款关闭
         */
        CLOSE,
        /**
         * 退款异常，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，可前往【商户平台—>交易中心】，手动处理此笔退款
         */
        ABNORMAL
    }

}
