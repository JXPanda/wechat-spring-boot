package com.jxpanda.starter.wechat;

import com.jxpanda.starter.wechat.property.WechatProperties;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.net.URI;

/**
 * 处理微信接口URL
 *
 * @author Panda
 */
public class WechatUrlService {

    @Resource
    private WechatProperties wechatProperties;

    public String userInfo(String accessToken, String openId) {
        return "https://api.weixin.qq.com/sns/userinfo?access_token=" + accessToken + "&openid=" + openId;
    }

    public String code2Session(String code) {
        WechatProperties.Application applet = wechatProperties.getApplet();
        return "https://api.weixin.qq.com/sns/jscode2session?appid=" + applet.getAppId() + "&secret=" + applet.getAppSecret() + "&js_code=" + code + "&grant_type=authorization_code";
    }

    public String accessToken() {
        WechatProperties.Application applet = wechatProperties.getApplet();
        return "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + applet.getAppId() + "&secret=" + applet.getAppSecret();
    }

    public String qrCodeUnlimited(String accessToken) {
        return "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + accessToken;
    }

    public String unifiedOrder() {
        return "https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi";
    }

    public URI queryOrder(String outTradeNo) {
        return URI.create("https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/" + outTradeNo + "?mchid=" + wechatProperties.getMerchant().getMchId());
    }

    public URI closeOrder(String outTradeNo) {
        return URI.create("https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/ " + outTradeNo + "/close");
    }

    public String refunds() {
        return "https://api.mch.weixin.qq.com/v3/refund/domestic/refunds";
    }

    public URI queryRefunds(String outTradeNo) {
        return URI.create("https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/" + outTradeNo);
    }

}
