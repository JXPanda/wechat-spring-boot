package com.jxpanda.starter.wechat.pojo.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Panda
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WechatPayer {

    @JsonProperty("openid")
    private String openId;

}
