package com.jxpanda.starter.wechat.pojo.pay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jxpanda.commons.constant.DecimalConstant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author Panda
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WechatPayAmount {

    private static final String CURRENCY_CNY = "CNY";

    @JsonProperty("currency")
    private String currency;
    @JsonProperty("total")
    private int total;
    @JsonProperty("refund")
    private int refund;
    @JsonProperty("payer_total")
    private int payerTotal;
    @JsonProperty("payer_refund")
    private int payerRefund;
    @JsonProperty("settlement_refund")
    private int settlementRefund;
    @JsonProperty("settlement_total")
    private int settlementTotal;
    @JsonProperty("discount_refund")
    private int discountRefund;
    @JsonProperty("payer_currency")
    private String payerCurrency;


    public static UnifiedOrderAmount unifiedOrderAmount(BigDecimal total) {
        WechatPayAmount.UnifiedOrderAmount unifiedOrderAmount = new WechatPayAmount.UnifiedOrderAmount();
        unifiedOrderAmount.total = transferDecimal(total);
        unifiedOrderAmount.currency = CURRENCY_CNY;
        return unifiedOrderAmount;
    }

    public static WechatPayAmount.RefundAmount refund(BigDecimal total, BigDecimal refund) {
        WechatPayAmount.RefundAmount refundAmount = new WechatPayAmount.RefundAmount();
        refundAmount.total = transferDecimal(total);
        refundAmount.refund = transferDecimal(refund);
        refundAmount.currency = CURRENCY_CNY;
        return refundAmount;
    }

    private static int transferDecimal(BigDecimal decimal) {
        return decimal.multiply(DecimalConstant.ONE_HUNDRED).intValue();
    }

    @Data
    public static class UnifiedOrderAmount {
        @JsonProperty("currency")
        private String currency;
        @JsonProperty("total")
        private int total;
    }

    @Data
    public static class RefundAmount {
        @JsonProperty("currency")
        private String currency;
        @JsonProperty("total")
        private int total;
        @JsonProperty("refund")
        private int refund;
    }
}
