package com.jxpanda.starter.wechat.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jxpanda.commons.constant.StringConstant;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Panda
 */
@Data
public class WechatUserData {

    @JsonProperty("watermark")
    private Watermark watermark;

    /**
     * 这个是用来解密当前数据用的session
     */
    private WechatResponse.Session session;

    /**
     * 这个字段是增加的，不是微信有的，方便判断这个对象是否有效
     */
    private boolean effective;

    /**
     * 小程序解出来的用户手机号
     */
    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class Phone extends WechatUserData {
        @JsonProperty("phoneNumber")
        private String phoneNumber;
        @JsonProperty("purePhoneNumber")
        private String purePhoneNumber;
        @JsonProperty("countryCode")
        private String countryCode;

    }

    /**
     * 小程序解出来的用户数据
     */
    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class Info extends WechatUserData {
        @JsonProperty("avatarUrl")
        private String avatar = StringConstant.BLANK;
        @JsonProperty("city")
        private String city = StringConstant.BLANK;
        @JsonProperty("country")
        private String country = StringConstant.BLANK;
        @JsonProperty("gender")
        private Integer gender = 0;
        @JsonProperty("nickName")
        private String nickname = StringConstant.BLANK;
        @JsonProperty("openId")
        private String openId = StringConstant.BLANK;
        @JsonProperty("province")
        private String province = StringConstant.BLANK;
        @JsonProperty("unionId")
        private String unionId = StringConstant.BLANK;

        /**
         * 由于微信改了userInfo的接口
         * 新的userInfo接口拿不到openId和unionId了
         * 这里可以从session对象中设置进去
         */
        @Override
        public void setSession(WechatResponse.Session session) {
            super.setSession(session);
            this.openId = session.getOpenId();
            this.unionId = session.getUnionId();
        }
    }

    /**
     * 检查一下水印是否有效
     */
    public boolean check(String appId) {
        this.effective = appId.equals(watermark.getAppId());
        return this.effective;
    }


    @Data
    public static class Watermark {
        @JsonProperty("appid")
        private String appId = StringConstant.BLANK;
        @JsonProperty("timestamp")
        private Long timestamp = 0L;
    }

}
